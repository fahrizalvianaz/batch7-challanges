package com.binar.binarfud.challange.repository.oauth;


import com.binar.binarfud.challange.model.entity.oauth.Client;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ClientRepository extends PagingAndSortingRepository<Client, Long> {

    Client findOneByClientId(String clientId);

}
