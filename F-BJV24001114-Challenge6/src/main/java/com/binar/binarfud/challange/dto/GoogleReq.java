package com.binar.binarfud.challange.dto;

import lombok.Data;

@Data
public class GoogleReq {

    private String accessToken;
}
