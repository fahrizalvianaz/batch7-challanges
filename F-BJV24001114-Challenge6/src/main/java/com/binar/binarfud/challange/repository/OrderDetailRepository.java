package com.binar.binarfud.challange.repository;

import com.binar.binarfud.challange.model.entity.Merchant;
import com.binar.binarfud.challange.model.entity.Order;
import com.binar.binarfud.challange.model.entity.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface OrderDetailRepository extends JpaRepository<OrderDetail, UUID>{

    List<OrderDetail> findAllByOrderCode(Order order);
}
