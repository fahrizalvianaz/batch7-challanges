package com.binar.binarfud.challange.service.impl;

import com.binar.binarfud.challange.dto.OrderReqDTO;
import com.binar.binarfud.challange.dto.ProductReqDTO;
import com.binar.binarfud.challange.model.entity.Merchant;
import com.binar.binarfud.challange.model.entity.Product;
import com.binar.binarfud.challange.repository.MerchantRepository;
import com.binar.binarfud.challange.repository.ProductRepository;
import com.binar.binarfud.challange.service.ProductService;
import com.binar.binarfud.challange.utils.Response;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.UUID;

@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final MerchantRepository merchantRepository;
    private final ProductRepository productRepository;
    private final Response response;
    @Override
    public Map addProduct(ProductReqDTO productReqDTO) {

        Merchant merchant = merchantRepository.findById(productReqDTO.getMerchantId()).orElseThrow(() -> new RuntimeException("Merchant not found"));
        Product product = new Product();
        product.setProductName(productReqDTO.getProductName());
        product.setPrice(productReqDTO.getPrice());
        product.setCategory(productReqDTO.getCategory());
        product.setMerchantId(merchant);

        return response.sukses(productRepository.save(product));
    }

    @Override
    public Map editProduct(UUID orderId) {
        return null;
    }

    @Override
    public Map getProductById(UUID productId) {

        Product product = productRepository.findById(productId).orElseThrow(() -> new RuntimeException("Product not found"));
        return response.sukses(product);
    }

    @Override
    public Map getAllProduct(
    ) {
        return response.sukses(productRepository.findAll());
    }

    @Override
    public Map deleteProductById(UUID productId) {
        return null;
    }
}
