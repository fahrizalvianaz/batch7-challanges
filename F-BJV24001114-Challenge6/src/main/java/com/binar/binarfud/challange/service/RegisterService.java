package com.binar.binarfud.challange.service;

import com.binar.binarfud.challange.dto.LoginModel;
import com.binar.binarfud.challange.dto.RegisterModel;

import java.util.Map;

public interface RegisterService {

    Map registerManual(RegisterModel objModel) ;

    Map registerByGoogle(RegisterModel objModel) ;

    public Map login(LoginModel objLogin);
}
