package com.binar.binarfud.challange.dto;

import com.binar.binarfud.challange.model.entity.Users;

import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class OrderReqDTO {

    private List<UUID> productId;
    private String destinationAddress;
    private UUID userId;
    private List<Integer> quantity;

}
