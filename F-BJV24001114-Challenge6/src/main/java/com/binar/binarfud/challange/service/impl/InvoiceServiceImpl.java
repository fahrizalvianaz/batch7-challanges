package com.binar.binarfud.challange.service.impl;

import com.binar.binarfud.challange.dto.OrderInvoiceJasperDTO;
import com.binar.binarfud.challange.model.entity.Order;
import com.binar.binarfud.challange.model.entity.OrderDetail;
import com.binar.binarfud.challange.repository.OrderDetailRepository;
import com.binar.binarfud.challange.repository.OrderRepository;
import com.binar.binarfud.challange.service.InvoiceService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.var;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.util.JRSaver;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;


@Service
@AllArgsConstructor
@Slf4j
public class InvoiceServiceImpl implements InvoiceService {

    private final OrderDetailRepository orderDetailRepository;
    private final OrderRepository orderRepository;
    @Override
    public byte[]  generateInvoice(UUID orderId) {
        List<OrderInvoiceJasperDTO> itemsInvoice = new ArrayList<>();
        double totalPrice = 0;
        Order order = orderRepository.findById(orderId).orElseThrow(() -> new RuntimeException("Order not found"));
        List<OrderDetail> items = orderDetailRepository.findAllByOrderCode(order);
        for(var i = 0; i < items.size(); i++){
            OrderInvoiceJasperDTO orderInvoiceJasperDTO = new OrderInvoiceJasperDTO();
            orderInvoiceJasperDTO.setProductName(items.get(i).getProductCode().getProductName());
            orderInvoiceJasperDTO.setPrice(items.get(i).getProductCode().getPrice());
            orderInvoiceJasperDTO.setQuantity(items.get(i).getQuantity());
            orderInvoiceJasperDTO.setTotalPrice(items.get(i).getTotalPrice());
            totalPrice += items.get(i).getTotalPrice();
            itemsInvoice.add(orderInvoiceJasperDTO);
        }
        JasperReport jasperReport;

        log.info("orderDetail => " + itemsInvoice);
        try {
            jasperReport = (JasperReport)
                    JRLoader.loadObject(ResourceUtils.getFile("Simple_Blue.jasper"));
        } catch (FileNotFoundException | JRException e) {
            try {
                File file = ResourceUtils.getFile("classpath:Simple_Blue.jrxml");
                jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
                JRSaver.saveObject(jasperReport, "Simple_Blue.jasper");
            } catch (FileNotFoundException | JRException ex) {
                throw new RuntimeException(e);
            }
        }
        String userName = order.getUserId().getUsername();
        String orderCode = order.getOrderId().toString();
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(itemsInvoice);
        Map<String, Object> parameters = new HashMap<>();

        parameters.put("userName", userName);
        parameters.put("orderId", orderCode);
        parameters.put("totalPrice", totalPrice);

        log.info("Order ID : {}", orderCode);
        log.info("User Name : {}", userName);
        log.info("Total Price : {}", totalPrice);

        JasperPrint jasperPrint = null;
        byte[] reportContent;

        try {
            jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);

            reportContent = JasperExportManager.exportReportToPdf(jasperPrint);

        } catch (JRException e) {
            throw new RuntimeException(e);
        }
        return reportContent;
    }

    @Override
    public Map generateReportingMerchant(UUID merchantId) {
        return null;
    }
}
