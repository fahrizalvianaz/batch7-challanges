package com.binar.binarfud.challange.controller;

import com.binar.binarfud.challange.dto.MerchantReqDTO;
import com.binar.binarfud.challange.service.MerchantService;
import com.binar.binarfud.challange.utils.GenericResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/merchant")
public class MerchantController {

    private final MerchantService merchantService;

    @PostMapping("/add")
    public GenericResponse<Object> addMerchant(@RequestBody MerchantReqDTO merchantRequest) {
        return GenericResponse.success(HttpStatus.OK, merchantService.addMerchant(merchantRequest), null);
    }

    @PutMapping("/edit/status/{merchantCode}")
    public GenericResponse<Object> editMerchant(
            @PathVariable("merchantCode") UUID merchantCode,
            @RequestParam(required = false) boolean isOpen) {
        return GenericResponse.success(HttpStatus.OK, merchantService.editStatusMerchant(merchantCode, isOpen), null);
    }


    @GetMapping("/merchants")
    public GenericResponse<Object> getMerchant(
            @RequestParam(required = false) boolean isOpen,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "20") int size
    ) {
        return GenericResponse.success(HttpStatus.OK, "Data berhasil diambil", merchantService.getMerchant(isOpen, page, size));
    }
}
