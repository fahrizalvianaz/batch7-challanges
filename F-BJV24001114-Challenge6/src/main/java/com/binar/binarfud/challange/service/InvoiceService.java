package com.binar.binarfud.challange.service;

import java.util.Map;
import java.util.UUID;

public interface InvoiceService {


    byte[]  generateInvoice(UUID orderId);

    Map generateReportingMerchant(UUID merchantId);


}
