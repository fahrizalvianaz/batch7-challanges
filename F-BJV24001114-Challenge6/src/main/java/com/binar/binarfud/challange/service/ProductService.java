package com.binar.binarfud.challange.service;

import com.binar.binarfud.challange.dto.OrderReqDTO;
import com.binar.binarfud.challange.dto.ProductReqDTO;

import java.util.Map;
import java.util.UUID;

public interface ProductService {

    Map addProduct(ProductReqDTO productReqDTO);

    Map editProduct(UUID orderId);

    Map getProductById(UUID productId);
    Map getAllProduct();

    Map deleteProductById(UUID productId);
}
