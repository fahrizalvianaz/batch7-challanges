package com.binar.binarfud.challange.model.entity;


import com.binar.binarfud.challange.model.entity.oauth.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;



import java.sql.Timestamp;
import java.util.UUID;

@Data
@Entity
@Table(name = "order", schema = "public")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID orderId;

    @Column(name = "order_time")
    @CreationTimestamp
    private Timestamp orderTime;

    @Column(name = "destination_address")
    private String destinationAddress;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User userId;

    private boolean isCompleted;

}
