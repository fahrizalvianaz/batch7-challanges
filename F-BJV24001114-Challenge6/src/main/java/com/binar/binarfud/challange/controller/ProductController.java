package com.binar.binarfud.challange.controller;


import com.binar.binarfud.challange.dto.OrderReqDTO;
import com.binar.binarfud.challange.dto.ProductReqDTO;
import com.binar.binarfud.challange.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.UUID;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/product")
public class ProductController {

    private final ProductService productService;

    @PostMapping(value = {"/add"})
    public ResponseEntity<Map> saveEmployee(@RequestBody ProductReqDTO request) {
        return new ResponseEntity<Map>(productService.addProduct(request), HttpStatus.OK);
    }
//
//    @PutMapping(value = {"/edit/{productId}", "/edit/{productId}/"})
//    public ResponseEntity<Map> editProduct(
//            @PathVariable UUID productId,
//            @RequestParam boolean isCompleted) {
//        return new ResponseEntity<Map>(orderId, isCompleted), HttpStatus.OK);
//    }
//
//    @GetMapping(value = {"/order/{productId}", })
//    public ResponseEntity<Map> getProductById(@PathVariable UUID productId) {
//        return new ResponseEntity<Map>(response.sukses(orderService.getAllOrder()), HttpStatus.OK);
//    }
//
//    @GetMapping(value = {"/orders", "/orders/"})
//    public ResponseEntity<Map> getAllProduct() {
//        return new ResponseEntity<Map>(response.sukses(orderService.getAllOrder()), HttpStatus.OK);
//    }
//
//    @DeleteMapping(value = {"/delete/{productId}", "/delete/{productId}/"})
//    public ResponseEntity<Map> deleteProductById(@PathVariable UUID productId) {
//        return new ResponseEntity<Map>(response.sukses(orderService.deteleOrder(orderId)), HttpStatus.OK);
//    }
}
