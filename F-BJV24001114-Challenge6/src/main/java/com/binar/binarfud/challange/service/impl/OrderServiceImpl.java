package com.binar.binarfud.challange.service.impl;

import com.binar.binarfud.challange.dto.OrderReqDTO;
import com.binar.binarfud.challange.exception.UnauthorizedException;
import com.binar.binarfud.challange.model.entity.Order;
import com.binar.binarfud.challange.model.entity.OrderDetail;
import com.binar.binarfud.challange.model.entity.Product;
import com.binar.binarfud.challange.model.entity.Users;
import com.binar.binarfud.challange.model.entity.oauth.User;
import com.binar.binarfud.challange.repository.OrderDetailRepository;
import com.binar.binarfud.challange.repository.OrderRepository;
import com.binar.binarfud.challange.repository.ProductRepository;
import com.binar.binarfud.challange.repository.UsersRepository;
import com.binar.binarfud.challange.repository.oauth.UserRepository;
import com.binar.binarfud.challange.service.OrderService;
import com.binar.binarfud.challange.utils.Response;
import lombok.AllArgsConstructor;
import lombok.var;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.UUID;

@Service
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final Response response;
    private final OrderRepository orderRepository;
    private final UsersRepository userRepository;
    private final UserRepository userRepositoryOuth;
    private final OrderDetailRepository orderDetailRepository;
    private final ProductRepository productRepository;
    @Override
    public Map createOrder(OrderReqDTO orderData) {
        Users user = userRepository.findById(orderData.getUserId())
                .orElseThrow(() -> new UnauthorizedException("User tidak ditemukan"));
        User userOuth = userRepositoryOuth.findOneByUsername(user.getEmailAddress());
        Order order = new Order();
        order.setDestinationAddress(orderData.getDestinationAddress());
        order.setUserId(userOuth);
        order.setCompleted(false);
        Order saveOrder = orderRepository.save(order);
        for(var i = 0; i < orderData.getProductId().size(); i++){
            Product product = productRepository.findById(orderData.getProductId().get(i))
                    .orElseThrow(() -> new UnauthorizedException("Product tidak ditemukan"));
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setOrderCode(saveOrder);
            orderDetail.setProductCode(product);
            orderDetail.setQuantity(orderData.getQuantity().get(i));
            orderDetail.setTotalPrice(product.getPrice() * orderData.getQuantity().get(i));
            orderDetailRepository.save(orderDetail);
        }
        return response.sukses(saveOrder);
    }


    @Override
    public Map editStatusOrder(UUID orderId, boolean isCompleted) {
        Order order = orderRepository.findById(orderId)
                .orElseThrow(() -> new UnauthorizedException("Order tidak ditemukan"));

        order.setCompleted(isCompleted);
        return response.sukses(orderRepository.save(order));
    }

    @Override
    public Map getAllOrder() {
        return response.sukses(orderRepository.findAll());
    }

    @Override
    public Map deteleOrder(UUID orderId) {
        Order order = orderRepository.findById(orderId)
                .orElseThrow(() -> new UnauthorizedException("Order tidak ditemukan"));
        orderRepository.delete(order);
        return response.sukses("Order berhasil dihapus");

    }
}
