package com.binar.binarfud.challange.repository;

import com.binar.binarfud.challange.model.entity.Merchant;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class MerchantRepositoryTest {

    @Autowired
    private MerchantRepository merchantRepository;


    @Test
    public void MerchantRepository_GetAllByOpenIs_ReturnMoreThanOne() {

        for(int i = 1; i <= 10; i++) {
            Merchant merchant = new Merchant();
            merchant.setMerchantName("Merchant " + i);
            merchant.setMerchantLocation("Jalan Merchant " + i);
            merchant.setOpen(i % 2 != 0);
            merchantRepository.save(merchant);
            merchantRepository.flush();
        }
        Pageable pageable = PageRequest.of(2 - 1, 3);
        Page<Merchant> merchats = merchantRepository.findAllByOpenIs(true, pageable);

        Assertions.assertNotNull(merchats);
        Assertions.assertEquals(3, merchats.getSize());

    }


}
