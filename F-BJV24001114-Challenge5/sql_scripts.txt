DROP PROCEDURE IF EXISTS insert_users;
DROP PROCEDURE IF EXISTS edit_users;
DROP PROCEDURE IF EXISTS delete_users;

CREATE OR REPLACE PROCEDURE insert_users(
    usernamePr varchar(255),
    emailPr varchar(255),
    passPr varchar(255)
) AS $$
BEGIN
    INSERT INTO users
        (id, username, email_address, password)
    VALUES
        (gen_random_uuid(), usernamePr, emailPr, passPr);
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE PROCEDURE edit_users(
    idPr uuid,
    usernamePr varchar(255),
    emailPr varchar(255),
    passPr varchar(255)
) AS $$
BEGIN
    IF usernamePr IS NULL THEN
        usernamePr := (SELECT username FROM users WHERE id = idPr);
    END IF;

    IF emailPr IS NULL THEN
        emailPr := (SELECT email_address FROM users WHERE id = idPr);
    END IF;

    IF passPr IS NULL THEN
        passPr := (SELECT password FROM users WHERE id = idPr);
    END IF;

    UPDATE users
    SET
        username = usernamePr,
        email_address = emailPr,
        password = passPr
    WHERE
        id = idPr;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE PROCEDURE delete_users(
    idPr uuid
) AS $$
BEGIN
    DELETE FROM users
    WHERE
        id = idPr;
END;
$$ LANGUAGE plpgsql;