package com.binar.binarfud.challange.service.impl;

import com.binar.binarfud.challange.config.BCrypt;
import com.binar.binarfud.challange.dto.UsersReqDTO;
import com.binar.binarfud.challange.exception.UnauthorizedException;
import com.binar.binarfud.challange.model.entity.Users;
import com.binar.binarfud.challange.repository.UsersRepository;
import com.binar.binarfud.challange.service.UserService;
import lombok.AllArgsConstructor;

import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {


    private final UsersRepository userRepository;

    @Override
    public String addUser(UsersReqDTO userRequest) {
        Users user = userRepository.findByEmailAddress(userRequest.getEmail());
        if(user != null) {
            throw new UnauthorizedException("Email sudah terdaftar");
        }

        String hashedPassword = BCrypt.hashpw(userRequest.getPassword(), BCrypt.gensalt());
        userRepository.insertUser(userRequest.getUsername(), userRequest.getEmail(), hashedPassword);
        return "User berhasil ditambahkan";
    }

    @Override
    public String editUser(UUID userCode, UsersReqDTO userRequest) {
        Users user = userRepository.findById(userCode)
                .orElseThrow(() -> new UnauthorizedException("User tidak ditemukan"));
        String hashedPassword = userRequest.getPassword() != null ? BCrypt.hashpw(userRequest.getPassword(), BCrypt.gensalt()) : null;
        userRepository.editUser(user.getId(), userRequest.getUsername(), userRequest.getEmail(), hashedPassword);
        return "User berhasil diupdate";
    }

    @Override
    public String deleteUser(UUID userCode) {
        Users user = userRepository.findById(userCode)
                .orElseThrow(() -> new UnauthorizedException("User tidak ditemukan"));
        userRepository.deleteUser(user.getId());
        return "User berhasil dihapus";
    }

}
