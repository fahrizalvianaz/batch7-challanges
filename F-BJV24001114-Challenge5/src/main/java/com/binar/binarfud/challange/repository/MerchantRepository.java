package com.binar.binarfud.challange.repository;

import com.binar.binarfud.challange.model.entity.Merchant;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface MerchantRepository extends JpaRepository<Merchant, UUID>{

    @Query("SELECT m FROM Merchant m WHERE m.isOpen = ?1")
    Page<Merchant> findAllByOpenIs(boolean isOpen, Pageable pageable);

    Merchant findByMerchantName(String merchantName);

}
