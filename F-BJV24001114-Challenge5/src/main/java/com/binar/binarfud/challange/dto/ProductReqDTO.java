package com.binar.binarfud.challange.dto;

import com.binar.binarfud.challange.model.enums.Category;
import lombok.Data;

import java.util.UUID;

@Data
public class ProductReqDTO {
    private String productName;
    private double price;
    private Category category;
    private UUID merchantId;
}
