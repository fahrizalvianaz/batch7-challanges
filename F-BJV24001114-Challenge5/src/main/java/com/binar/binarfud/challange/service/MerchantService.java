package com.binar.binarfud.challange.service;

import com.binar.binarfud.challange.dto.MerchantReqDTO;
import com.binar.binarfud.challange.model.entity.Merchant;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface MerchantService {

    String addMerchant(MerchantReqDTO merchantRequest);

    String editStatusMerchant(UUID merchantCode, boolean isOpen);

    List<Merchant> getMerchant(boolean isOpen, int page, int size);
}
