package com.binar.binarfud.challange.dto;

import jakarta.persistence.Column;
import lombok.Builder;
import lombok.Data;

@Data
public class OrderInvoiceJasperDTO {

    private String productName;
    private double price;
    private int quantity;
    private double totalPrice;

}
