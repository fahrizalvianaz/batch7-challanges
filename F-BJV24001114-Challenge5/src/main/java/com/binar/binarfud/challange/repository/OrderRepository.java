package com.binar.binarfud.challange.repository;

import com.binar.binarfud.challange.model.entity.Merchant;
import com.binar.binarfud.challange.model.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface OrderRepository extends JpaRepository<Order, UUID>{


}
