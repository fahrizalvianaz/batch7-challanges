package com.binar.binarfud.challange.repository;

import com.binar.binarfud.challange.model.entity.OrderDetail;
import com.binar.binarfud.challange.model.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ProductRepository extends JpaRepository<Product, UUID>{


}
