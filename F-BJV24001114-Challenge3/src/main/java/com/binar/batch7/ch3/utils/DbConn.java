package com.binar.batch7.ch3.utils;

import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.DriverManager;

@Slf4j
public class DbConn {
    public Connection connectDb(String db_name, String user, String pass) {
        Connection conn = null;
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection( "jdbc:postgresql://localhost:5432/"+ db_name,user,pass);
            log.info("Koneksi ke SQLite berhasil");

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }
}
