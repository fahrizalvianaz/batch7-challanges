package com.binar.batch7.ch3.main;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MenuList {

    private String name;
    private Integer price;
}
