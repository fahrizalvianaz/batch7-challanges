package com.binar.batch7.ch3.utils.dao;

import com.binar.batch7.ch3.dto.MenuDto;
import jdk.jfr.Category;

import java.util.List;

public interface DaoService {

    List<MenuDto> showData();
    void insertData(String name, int harga, int stock, int category_id);
    void updateData(int id, String newName, int column);
    void deleteData(int id);
}
