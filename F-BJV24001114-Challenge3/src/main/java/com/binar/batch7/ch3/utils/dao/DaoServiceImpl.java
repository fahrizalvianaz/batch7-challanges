package com.binar.batch7.ch3.utils.dao;

import com.binar.batch7.ch3.dto.MenuDto;
import com.binar.batch7.ch3.utils.DbConn;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DaoServiceImpl implements DaoService{


    @Override
    public List<MenuDto> showData() {
        DbConn dbConn = new DbConn();
        List<MenuDto> menus = new ArrayList<>();
        try(Connection conn = dbConn.connectDb("binarfud", "postgres", "fahrizal123");) {
            ResultSet rs = conn.createStatement().executeQuery("SELECT id, nama_menu, harga FROM menu");
            while (rs.next()) {
                MenuDto menu = new MenuDto();
                menu.setId(rs.getLong("id"));
                menu.setMenu(rs.getString("nama_menu"));
                menu.setHarga(rs.getInt("harga"));
                menus.add(menu);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return menus;
    }

    @Override
    public void insertData(String name, int harga, int stock, int category_id) {
        String category;
        if(category_id == 1){
            category = "makanan";
        }else if(category_id == 2){
            category = "minuman";
        } else {
            category = "snack";
        }
        DbConn dbConn = new DbConn();
        try(Connection conn = dbConn.connectDb("binarfud", "postgres", "fahrizal123");) {
            conn.createStatement().executeUpdate("INSERT INTO menu (nama_menu, harga, stok, category) VALUES ('"+name+"', "+harga+", "+stock+", '"+category+"')");
            System.out.println("Data berhasil ditambahkan");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    @Override
    public void updateData(int id, String newData, int pilihColumn) {
        DbConn dbConn = new DbConn();
        try(Connection conn = dbConn.connectDb("binarfud", "postgres", "fahrizal123");) {
            if(pilihColumn == 1) {
                conn.createStatement().executeUpdate("UPDATE menu SET nama_menu = '"+newData +"',updatedat = CURRENT_TIMESTAMP WHERE id = "+id);
            } else if(pilihColumn == 2) {
                conn.createStatement().executeUpdate("UPDATE menu SET harga = '"+newData +"',updatedat = CURRENT_TIMESTAMP WHERE id = "+id);
            } else if(pilihColumn == 3) {
                conn.createStatement().executeUpdate("UPDATE menu SET stok = '"+newData +"', updatedat = CURRENT_TIMESTAMP WHERE id = "+id);
            } else {
                String category;

                if(Integer.parseInt(newData) == 1){
                    category = "makanan";
                }else if(Integer.parseInt(newData) == 2){
                    category = "minuman";
                } else {
                    category = "snack";
                }
                conn.createStatement().executeUpdate("UPDATE menu SET category = '"+category +"' WHERE id = "+id);
            }
            System.out.println("Data berhasil diubah");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void deleteData(int id) {

        DbConn dbConn = new DbConn();
        try(Connection conn = dbConn.connectDb("binarfud", "postgres", "fahrizal123");) {
            conn.createStatement().executeUpdate("DELETE FROM menu WHERE id = "+id);
            System.out.println("Data berhasil dihapus");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
}
