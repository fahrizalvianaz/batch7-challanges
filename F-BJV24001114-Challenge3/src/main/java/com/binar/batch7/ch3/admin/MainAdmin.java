package com.binar.batch7.ch3.admin;

import com.binar.batch7.ch3.utils.dao.DaoService;
import com.binar.batch7.ch3.utils.dao.DaoServiceImpl;


import java.util.Scanner;

public class MainAdmin {

    static DaoService daoService = new DaoServiceImpl();
    public static void main(String[] args) {
        boolean isUlang = true;
        do{


        Scanner input = new Scanner(System.in);
        System.out.println("\n\n1. Insert Data");
        System.out.println("2. Update Data");
        System.out.println("3. Delete Data");
        System.out.println("4. Show Data");
        System.out.println("0. Keluar");
        System.out.print("=> ");
        int pilih = input.nextInt();
        if( pilih == 1) {
            input.nextLine();
            System.out.print("Masukkan nama menu: ");
            String namaMenu = input.nextLine();
            System.out.print("Masukkan harga: ");
            int harga = input.nextInt();
            System.out.print("Masukkan stok: ");
            int stok = input.nextInt();
            System.out.print("Pilih kategori: ");
            System.out.println("\n1. Makanan");
            System.out.println("2. Minuman");
            System.out.println("3. Snack");
            System.out.print("=> ");
            int category = input.nextInt();
            daoService.insertData(namaMenu, harga, stok, category);
        } else if (pilih == 2) {
            showMenu();
            System.out.print("Pilih id menu yang akan di update: ");
            int id = input.nextInt();
            input.nextLine();
            System.out.print("Pilih variable yang akan di update: ");
            System.out.println("\n1. Nama menu");
            System.out.println("2. Harga");
            System.out.println("3. Stok");
            System.out.println("4. Category");
            System.out.print("=> ");
            int pilihColumn = input.nextInt();
            input.nextLine();
            if(pilihColumn == 1) {
                System.out.print("Masukkan nama menu baru: ");
                String newData = input.nextLine();
                daoService.updateData(id, newData, pilihColumn);
            } else if(pilihColumn == 2) {
                System.out.print("Masukkan harga baru: ");
                String newData = input.nextLine();
                daoService.updateData(id, newData, pilihColumn);
            } else if(pilihColumn == 3) {
                System.out.print("Masukkan stok baru: ");
                String newData = input.nextLine();
                daoService.updateData(id, newData, pilihColumn);
            } else if(pilihColumn == 4) {
                System.out.print("Masukkan category baru: ");
                System.out.println("\n1. Makanan");
                System.out.println("2. Minuman");
                System.out.println("3. Snack");
                System.out.print("=> ");
                String newData = input.nextLine();
                daoService.updateData(id, newData, pilihColumn);

            } else {
                System.out.println("Pilihan tidak tersedia");
            }

        } else if (pilih == 3) {
            showMenu();
            System.out.print("Pilih id menu yang akan di hapus: ");
            int id = input.nextInt();
            daoService.deleteData(id);
        } else if (pilih == 4) {
            showMenu();
        } else if (pilih == 0) {
            System.out.println("Sistem di matikan");
            isUlang = false;
        } else {
            System.out.println("Pilihan tidak tersedia");

        }
        input.nextLine();
        }while (isUlang);

    }
    public static void showMenu() {
        daoService.showData()
                .stream()
                .map(menu -> menu.getId()+ ". "+ menu.getMenu() + " " + menu.getHarga())
                .forEach(System.out::println);
    }
}
