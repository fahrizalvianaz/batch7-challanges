package com.binar.batch7.ch3.dto;

import lombok.Data;

@Data
public class MenuDto {
    private long id;
    private String menu;
    private int harga;
}
