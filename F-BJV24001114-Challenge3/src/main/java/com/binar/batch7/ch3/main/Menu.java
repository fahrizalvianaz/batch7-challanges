package com.binar.batch7.ch3.main;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@NoArgsConstructor
@AllArgsConstructor
public class Menu {

    private String menu;
    private int harga;


}
