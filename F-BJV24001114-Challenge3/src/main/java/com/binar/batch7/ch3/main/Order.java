package com.binar.batch7.ch3.main;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Scanner;


@Data
@NoArgsConstructor
public class Order {

    private MenuList listMenu;
    private int totalHarga;
    private int totalQty;

    Order(MenuList listMenu){
        this.listMenu = listMenu;
    }

    public String getOrder() {
        Scanner input = new Scanner(System.in);

        String header = """
                ======================
                Berapa pesanan anda
                ======================
                """;
        String bottom = "(input 0 untuk kembali)\n";

        System.out.println(header);
        System.out.println(this.listMenu.getName() + "  | Rp. " + this.listMenu.getPrice());
        System.out.print(bottom);
        System.out.print("qty: ");
        int qty = input.nextInt();
        if (qty != 0) {
            this.totalHarga = this.totalHarga + qty * listMenu.getPrice();
            this.totalQty = this.totalQty + qty;
            return (listMenu.getName() +"\t\t"+ qty +"\t\t"+  "Rp. " + this.totalHarga);
        }
        return "";
    }

}
