package com.binar.batch7.ch3.main;

import com.binar.batch7.ch3.dto.MenuDto;
import com.binar.batch7.ch3.utils.dao.DaoService;
import com.binar.batch7.ch3.utils.dao.DaoServiceImpl;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Challenge3 {
    public static List<String> pesanan = new ArrayList<>();
    public static List<MenuList> listMenu = new ArrayList<>();
    public static List<Integer> totalHarga = new ArrayList<>();
    public static List<Integer> totalQty = new ArrayList<>();
    public static boolean isPesan = false;

    public static DaoService daoService = new DaoServiceImpl();


    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        Optional<List<MenuDto>> menuList2 = Optional.of(Optional.ofNullable(daoService.showData())
                .orElseGet(() -> {
                    System.out.println("Data kosong");
                    return new ArrayList<>();
                }));

        do {

            String menuMakanan = """
                ==========================
                Selamat datang di BinarFud
                ==========================

                """;
            System.out.print(menuMakanan);
            System.out.println("Silahkan pilih makanan : ");
            for(var i = 0; i < menuList2.get().size(); i++) {
                System.out.println(menuList2.get().get(i).getId() + ". " + menuList2.get().get(i).getMenu() + " " + menuList2.get().get(i).getHarga());
                menu(menuList2.get().get(i).getMenu(), menuList2.get().get(i).getHarga());
            }
            System.out.println("99. Pesan dan bayar");
            System.out.println("0. Keluar");
            System.out.print("\n=> ");
            int pilihan = input.nextInt();

            if(pilihan > 0 && pilihan <= menuList2.get().size()){
                addOrder(listMenu.get(pilihan-1));
            } else if(pilihan == 99) {
                int sumHarga = totalHarga.stream().mapToInt(Integer::intValue).sum();
                int sumQty = totalQty.stream().mapToInt(Integer::intValue).sum();
                Konfirmasi konfirmasi = new Konfirmasi(pesanan, sumHarga, sumQty);
                isPesan = konfirmasi.getKonfirmasi();
            } else if (pilihan == 0) {
                isPesan = false;
            } else {
                System.out.println("Pilihan tidak tersedia");
            }

        } while (isPesan);
    }

    public static void addOrder(MenuList menu) {
        Order order = new Order(menu);
        pesanan.add(order.getOrder());
        totalHarga.add(order.getTotalHarga());
        totalQty.add(order.getTotalQty());
        isPesan = true;
    }

    public static void menu(String menu, int harga){
        MenuList menuInstance = new MenuList(menu, harga);
        listMenu.add(menuInstance);
    }

}