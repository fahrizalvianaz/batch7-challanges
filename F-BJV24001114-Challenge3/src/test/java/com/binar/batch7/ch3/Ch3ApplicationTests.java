package com.binar.batch7.ch3;

import com.binar.batch7.ch3.main.MenuList;
import com.binar.batch7.ch3.main.Order;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class Ch3ApplicationTests {


	private MenuList menu;
	private Order order;

	@BeforeEach
	public void setUp() {
		menu = new MenuList("Nasi Goreng", 15000);
		order = new Order();
		order.setListMenu(menu);
	}
	@Test
	public void testGetOrderWithValidInput() {
		System.setIn(new java.io.ByteArrayInputStream("5\n".getBytes()));

		String result = order.getOrder();

		Assertions.assertEquals("Nasi Goreng\t\t5\t\tRp. 75000", result);
		Assertions.assertEquals(75000, order.getTotalHarga());
		Assertions.assertEquals(5, order.getTotalQty());
	}

	@Test
	public void testGetOrderWithZeroInput() {

		System.setIn(new java.io.ByteArrayInputStream("0\n".getBytes()));

		String result = order.getOrder();

		Assertions.assertEquals("", result);
		Assertions.assertEquals(0, order.getTotalHarga());
		Assertions.assertEquals(0, order.getTotalQty());
	}


}
