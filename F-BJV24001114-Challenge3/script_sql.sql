
DROP TABLE IF EXISTS "menu_order";
DROP TABLE IF EXISTS "order";
DROP TABLE IF EXISTS "menu";
DROP TABLE IF EXISTS "orderdetail";
DROP TABLE IF EXISTS "orders";
DROP TABLE IF EXISTS "product";
DROP TABLE IF EXISTS "merchant";
DROP TABLE IF EXISTS "users";





DROP TABLE IF EXISTS "orders";

DROP TYPE IF EXISTS category;

CREATE TYPE category AS ENUM ('minuman', 'makanan', 'snack');



CREATE TABLE "menu"(
                       id   SERIAL  NOT NULL,
                       nama_menu VARCHAR (200) NOT NULL,
                       harga  int not null,
                       stok  int,
                       category category,
                       createdAt timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
                       updatedAt timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
                       PRIMARY KEY (id));

CREATE TABLE "order"(
                        id   SERIAL  NOT NULL,
                        nama VARCHAR (200) NOT NULL,
                        createdAt timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
                        PRIMARY KEY (id));

CREATE TABLE "menu_order"(
                             id   SERIAL  NOT NULL,
                             id_order int not null,
                             id_menu int not null,
                             createdAt timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
                             PRIMARY KEY (id),
                             FOREIGN KEY (id_order) REFERENCES public.order(id),
                             FOREIGN KEY (id_menu) REFERENCES menu(id));


INSERT INTO menu (
    nama_menu,
    harga,
    stok,
    category)
VALUES
    ('Nasi Goreng', 25000, 10, 'makanan'),
    ('Ayam Goreng', 30000, 15, 'makanan'),
    ('Es Teh', 8000, 20, 'minuman'),
    ('Sate Ayam', 35000, 12, 'makanan');

INSERT INTO "order" (nama)
VALUES
    ('Budi'),
    ('Andi'),
    ('Caca');


INSERT INTO menu_order (id_order, id_menu)
VALUES
    (1, 1),
    (1, 3),
    (2, 2),
    (2, 4),
    (3, 1),
    (3, 2),
    (3, 3),
    (3, 4);

UPDATE menu SET nama_menu = 'Es Teh Manis' WHERE id = 3;
UPDATE menu SET stok = 5 WHERE id = 1;
UPDATE menu SET stok = 10 WHERE id = 2;
UPDATE menu SET stok = 15 WHERE id = 3;
UPDATE menu SET stok = 8 WHERE id = 4;


SELECT
    "menu".id AS menu_id,
    "menu".nama_menu,
    "menu".harga,
    "menu".category,
    "order".id AS order_id,
    "order".nama,
    "menu_order".createdAt AS menu_order_createdAt
FROM
    "menu"
        INNER JOIN
    "menu_order" ON "menu".id = "menu_order".id_menu
        INNER JOIN
    "order" ON "menu_order".id_order = "order".id;





CREATE TABLE "users"(
                        id   SERIAL  NOT NULL,
                        username  varchar(200) not null,
                        email_address  varchar(200) not null,
                        password  varchar(200) not null,
                        complete boolean,
                        PRIMARY KEY (id));

CREATE TABLE "merchant"(
                           id   SERIAL  NOT NULL,
                           merchant_name  varchar(200) not null,
                           merchant_location  varchar(200) not null,
                           open boolean,
                           PRIMARY KEY (id));


CREATE TABLE "orders"(
                         id   SERIAL  NOT NULL,
                         order_time timestamp with time zone DEFAULT CURRENT_TIMESTAMP,
                         destination_address  varchar(200) not null,
                         user_id  int,
                         complete boolean,
                         PRIMARY KEY (id),
                         FOREIGN KEY (user_id) REFERENCES users(id));

CREATE TABLE "product"(
                          id   SERIAL  NOT NULL,
                          product_name  varchar(200) not null,
                          price  int,
                          merchant_id  int,
                          PRIMARY KEY (id),
                          FOREIGN KEY (merchant_id) REFERENCES merchant(id));

CREATE TABLE "orderdetail"(
                              id   SERIAL  NOT NULL,
                              order_id  int,
                              product_id  int,
                              quantity  int,
                              total_price  int,
                              PRIMARY KEY (id),
                              FOREIGN KEY (order_id) REFERENCES orders(id),
                              FOREIGN KEY (product_id) REFERENCES product(id));



