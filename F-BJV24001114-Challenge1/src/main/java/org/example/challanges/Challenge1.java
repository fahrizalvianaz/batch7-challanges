package org.example.challanges;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Challenge1 {


    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int totalHarga = 0;
        int totalQty = 0;
        boolean isPesan = false;
        ArrayList pesanan = new ArrayList();

        do {
            String strMainMenu = mainMenu();
            System.out.println(strMainMenu);

            System.out.print("\n=> ");
            int pilihan = input.nextInt();

            StringBuilder sb = new StringBuilder();
            sb.append("======================\n");
            sb.append("Berapa pesanan anda\n");
            sb.append("======================\n");

            String header = sb.toString();
            String bottom = "(input 0 untuk kembali)\n";

            switch (pilihan) {

                case 1:
                    System.out.println(header);
                    System.out.println("Nasi Goreng   | Rp. 15.000");
                    System.out.print(bottom);
                    System.out.print("\nqty: ");
                    int qty1 = input.nextInt();
                    if (qty1 == 0) {
                        isPesan = true;
                    } else {
                        totalHarga =totalHarga + qty1 * 15000;
                        totalQty = totalQty + qty1;
                        pesanan.add("Nasi Goreng     " + qty1 + "     15.000 ");
                        isPesan = true;
                    }
                    break;
                case 2:
                    System.out.println(header);
                    System.out.println("Mie Goreng    | Rp. 13.000");
                    System.out.print(bottom);
                    System.out.print("\nqty: ");
                    int qty2 = input.nextInt();
                    if (qty2 == 0) {
                        isPesan = true;
                    }
                    else {
                        totalHarga =totalHarga + qty2 * 13000;
                        totalQty = totalQty + qty2;
                        pesanan.add("Mie Goreng      " + qty2 + "     13.000 ");
                        isPesan = true;
                    }
                    break;
                case 3:
                    System.out.println(header);
                    System.out.println("Nasi + ayam   | Rp. 18.000");
                    System.out.print(bottom);
                    System.out.print("\nqty: ");
                    int qty3 = input.nextInt();
                    if (qty3 == 0) {
                        isPesan = true;
                    }
                    else {
                        totalHarga = totalHarga + qty3 * 18000;
                        totalQty = totalQty + qty3;
                        pesanan.add("Nasi + ayam     " + qty3 + "     18.000 ");
                        isPesan = true;
                    }
                    break;
                case 4:
                    System.out.println(header);
                    System.out.println("Es teh manis  | Rp. 3.000");
                    System.out.print(bottom);
                    System.out.print("\nqty: ");
                    int qty4 = input.nextInt();
                    if (qty4 == 0) {
                        isPesan = true;
                    }
                    else {
                        totalHarga = totalHarga + qty4 * 3000;
                        totalQty = totalQty + qty4;
                        pesanan.add("Es teh manis    " + qty4 + "     3.000 ");
                        isPesan = true;
                    }
                    break;
                case 5:
                    System.out.println(header);
                    System.out.println("Es Jeruk      | Rp. 5.000");
                    System.out.print(bottom);
                    System.out.print("\nqty: ");
                    int qty5 = input.nextInt();
                    if (qty5 == 0) {
                        isPesan = true;
                    }
                    else {
                        totalHarga = totalHarga + qty5 * 5000;
                        totalQty = totalQty + qty5;
                        pesanan.add("Es Jeruk        " + qty5 + "     5.000 ");
                        isPesan = true;
                    }
                    break;
                case 99:
                    StringBuilder konfirmasi = new StringBuilder();
                    konfirmasi.append("=======================\n");
                    konfirmasi.append("Konfirmasi & Pembayaran\n");
                    konfirmasi.append("=======================\n\n");
                    System.out.println(konfirmasi);
                    for (Object o : pesanan) {
                        System.out.println(o);
                    }
                    System.out.println("-------------------------------------+");
                    System.out.println("Total           " + totalQty + "     "+ totalHarga);
                    System.out.println("\n\n1.Konfirmasi dan Bayar");
                    System.out.println("2.Kembali ke menu utama");
                    System.out.println("0.Keluar aplikasi");
                    System.out.print("=> ");
                    int pilihKonfirmasi = input.nextInt();
                    if (pilihKonfirmasi == 1) {

                        String filePath = "struk.txt";
                        try  {
                            File file = new File(filePath);
                            FileWriter fileWriter = new FileWriter(file);
                            BufferedWriter writer = new BufferedWriter(fileWriter);
                            writer.write("=========================================\n");
                            writer.write("BinarFud\n");
                            writer.write("=========================================\n\n");
                            writer.write("Terima kasih sudah memesan di BinarFud\n\n");
                            writer.write("Dibawah ini adalah pesanan anda\n\n");
                            for (Object o : pesanan) {
                                writer.write(o + "\n");
                            }
                            writer.write("-----------------------------------------+\n");
                            writer.write("Total           " + totalQty + "     "+ totalHarga + "\n");
                            writer.write("\nPembayaran : BinarCash\n\n");
                            writer.write("=========================================\n");
                            writer.write("Simpan struk ini sebagai bukti pembayaran\n");
                            writer.write("=========================================\n\n");
                            writer.flush();
                            writer.close();
                            System.out.println("Struk berhasil di cetak.");
                        } catch (IOException e) {
                            System.err.println("Terjadi kesalahan saat menulis ke file: " + e.getMessage());
                        }

                        System.out.println("Terima kasih telah berkunjung");
                        isPesan = false;
                    } else if (pilihKonfirmasi == 2) {
                        isPesan = true;
                    } else if (pilihKonfirmasi == 0) {
                        System.out.println("Terima kasih telah berkunjung");
                        pesanan.clear();
                        isPesan = false;
                    } else {
                        System.out.println("Pilihan tidak tersedia");
                    }
                    break;
                case 0:
                    System.out.println("Terima kasih telah berkunjung");
                    break;
                default:
                    System.out.println("Pilihan tidak tersedia");
                    break;

            }

        } while (isPesan);

    }

    private static String mainMenu() {

        return "==========================\n" +
                "Selamat datang di BinarFud\n" +
                "==========================\n" +
                "\nSilahkan pilih makanan : " +
                "\n1. Nasi Goreng   | Rp. 15.000" +
                "\n2. Mie Goreng    | Rp. 13.000" +
                "\n3. Nasi + Ayam   | Rp. 18.000" +
                "\n4. Es teh Manis  | Rp. 3.000" +
                "\n5. Es Jeruk      | Rp. 5.000" +
                "\n99. Pesan dan bayar" +
                "\n0. Keluar";
    }

}