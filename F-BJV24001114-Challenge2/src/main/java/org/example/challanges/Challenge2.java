package org.example.challanges;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Challenge2 {
    public static List<String> pesanan = new ArrayList<>();
    public static List<MenuList> listMenu = new ArrayList<>();
    public static List<Integer> totalHarga = new ArrayList<>();
    public static List<Integer> totalQty = new ArrayList<>();
    public static boolean isPesan = false;


    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
//        Menu menu = new Menu();
        List<MenuList> menuList = Arrays.asList(
                new MenuList("Nasi Goreng", 15000),
                new MenuList("Mie Goreng", 12000),
                new MenuList("Nasi + Ayam", 18000),
                new MenuList("Es Teh Manis", 3000),
                new MenuList("Es Jeruk", 5000)

        );


        do {

            Optional<Map<String, Integer>> menuMap = Optional.of(menuList.stream()
                    .collect(Collectors.toMap(MenuList::getName, MenuList::getPrice)));
//            menu.getMenu(listMenu);
            AtomicInteger i = new AtomicInteger();
            if(menuMap.isPresent()) {
                menuMap.get().forEach((name, price) -> {
                    i.getAndIncrement();
                    System.out.println(i +". "+ name + "\t\t | \t\t" + price);
                    menu(name, price);
                });
                System.out.println("99. Pesan dan bayar");
                System.out.println("0. Keluar");
            }

            System.out.print("\n=> ");
            int pilihan = input.nextInt();

            if(listMenu.stream().anyMatch(menu -> menuList.indexOf(menu) == pilihan - 1)){
                addOrder(listMenu.get(pilihan-1));
            } else if(pilihan == 99) {
                int sumHarga = totalHarga.stream().mapToInt(Integer::intValue).sum();
                int sumQty = totalQty.stream().mapToInt(Integer::intValue).sum();
                Konfirmasi konfirmasi = new Konfirmasi(pesanan, sumHarga, sumQty);
                isPesan = konfirmasi.getKonfirmasi();
            } else if (pilihan == 0) {
                isPesan = false;
            } else {
                System.out.println("Pilihan tidak tersedia");
            }

        } while (isPesan);
    }

    public static void addOrder(MenuList menu) {
        Order order = new Order(menu);
        pesanan.add(order.getOrder());
        totalHarga.add(order.getTotalHarga());
        totalQty.add(order.getTotalQty());
        isPesan = true;
    }

    public static void menu(String menu, int harga){
        MenuList menuInstance = new MenuList(menu, harga);
        listMenu.add(menuInstance);
    }

}