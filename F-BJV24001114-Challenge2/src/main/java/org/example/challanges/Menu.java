package org.example.challanges;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class Menu {

    private String menu;
    private int harga;


    public void getMenu(List<Menu> menu){
        String menuMakanan = """
                ==========================
                Selamat datang di BinarFud
                ==========================

                """;
        System.out.print(menuMakanan);
        System.out.println("Silahkan pilih makanan : ");
        for(var i = 0; i < menu.size(); i++){
            System.out.println((i+1) + ". " + menu.get(i).menu + "\t\t| Rp. " + menu.get(i).harga);
        }
        System.out.println("99. Pesan dan bayar");
        System.out.println("0. Keluar");

    }

}
