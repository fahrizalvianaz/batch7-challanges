package org.example.challanges;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class Konfirmasi {

    private final int totalHarga;
    private final int totalQty;
    private final List<String> pesanan;

    Konfirmasi(List<String> pesanan, int totalHarga, int totalQty){
        this.pesanan = pesanan;
        this.totalHarga = totalHarga;
        this.totalQty = totalQty;
    }

    public boolean getKonfirmasi() {
        Scanner input = new Scanner(System.in);
        String konfirmasi = """
                =======================
                Konfirmasi & Pembayaran
                =======================

                """;
        System.out.println(konfirmasi);
        for (Object o : this.pesanan) {
            System.out.println(o);
        }
        System.out.println("-------------------------------------+");
        System.out.println("Total           " + this.totalQty + "\t\t" +  "Rp. " +this.totalHarga);
        System.out.println("\n\n1.Konfirmasi dan Bayar");
        System.out.println("2.Kembali ke menu utama");
        System.out.println("0.Keluar aplikasi");
        System.out.print("=> ");
        int pilihKonfirmasi = input.nextInt();
        if (pilihKonfirmasi == 1) {
            String filePath = "struk.txt";
            try {
                BufferedWriter writer = getBufferedWriter(filePath);
                writer.close();
                this.pesanan.clear();
                System.out.println("Struk berhasil di cetak.");
            } catch (IOException e) {
                System.err.println("Terjadi kesalahan saat menulis ke file: " + e.getMessage());
            } finally {
                return false;
            }
        } else if (pilihKonfirmasi == 2) {
            return true;
        } else if (pilihKonfirmasi == 0) {
            System.out.println("Terima kasih telah berkunjung");
            this.pesanan.clear();
            return false;
        } else {
            System.out.println("Pilihan tidak tersedia");
            return true;
        }
    }
    private BufferedWriter getBufferedWriter(String filePath) throws IOException {
        File file = new File(filePath);
        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter writer = new BufferedWriter(fileWriter);
        writer.write("=========================================\n");
        writer.write("BinarFud\n");
        writer.write("=========================================\n\n");
        writer.write("Terima kasih sudah memesan di BinarFud\n\n");
        writer.write("Dibawah ini adalah pesanan anda\n\n");
        for (Object o : pesanan) {
            writer.write(o + "\n");
        }
        writer.write("-----------------------------------------+\n");
        writer.write("Total           " + this.totalQty + "\t\t" +  "Rp. " + this.totalHarga + "\n");
        writer.write("\nPembayaran : BinarCash\n\n");
        writer.write("=========================================\n");
        writer.write("Simpan struk ini sebagai bukti pembayaran\n");
        writer.write("=========================================\n\n");
        writer.flush();
        return writer;
    }
}



