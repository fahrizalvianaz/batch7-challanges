package com.binar.binarfud.users.dto;

import lombok.Data;

@Data
public class GoogleReq {

    private String accessToken;
}
