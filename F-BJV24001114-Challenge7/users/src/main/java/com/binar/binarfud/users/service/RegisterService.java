package com.binar.binarfud.users.service;

import com.binar.binarfud.users.dto.LoginModel;
import com.binar.binarfud.users.dto.RegisterModel;

import java.util.Map;

public interface RegisterService {

    Map registerManual(RegisterModel objModel) ;

    Map registerByGoogle(RegisterModel objModel) ;

    public Map login(LoginModel objLogin);
}
