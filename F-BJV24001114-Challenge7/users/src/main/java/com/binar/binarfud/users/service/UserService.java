package com.binar.binarfud.users.service;

import com.binar.binarfud.users.dto.UsersReqDTO;

import java.security.Principal;
import java.util.Map;
import java.util.UUID;

public interface UserService {

    String addUser(UsersReqDTO userRequest);

    String editUser(UUID userCode, UsersReqDTO userRequest);

    String deleteUser(UUID userCode);

    public Map getDetailProfile(Principal principal);

}
