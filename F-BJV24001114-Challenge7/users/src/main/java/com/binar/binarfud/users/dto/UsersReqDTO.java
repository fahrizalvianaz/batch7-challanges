package com.binar.binarfud.users.dto;


import lombok.Data;

@Data
public class UsersReqDTO {

    private String username;

    private String password;

    private String email;


}
