package com.binar.binarfud.users.advice;

import com.binar.binarfud.users.exception.BadRequestException;
import com.binar.binarfud.users.exception.UnauthorizedException;
import com.binar.binarfud.users.utils.GenericResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ResponseErrorHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BadRequestException.class)
    public GenericResponse<Object> handleBadRequestException(BadRequestException ex) {
        return GenericResponse.error(HttpStatus.BAD_REQUEST, ex.getMessage());
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(UnauthorizedException.class)
    public GenericResponse<Object> handleUnautorization(UnauthorizedException ex) {
        return GenericResponse.error(HttpStatus.UNAUTHORIZED, ex.getMessage());
    }


}
