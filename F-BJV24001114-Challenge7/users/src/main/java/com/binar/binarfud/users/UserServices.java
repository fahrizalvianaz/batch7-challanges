package com.binar.binarfud.users;

import com.binar.binarfud.users.controller.fileupload.FileStorageProperties;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@OpenAPIDefinition
@EnableConfigurationProperties({
		FileStorageProperties.class
})
public class UserServices {
	public static void main(String[] args) {
		SpringApplication.run(UserServices.class, args);
	}

}
