package com.binar.binarfud.users.controller;


import com.binar.binarfud.users.dto.UsersReqDTO;
import com.binar.binarfud.users.service.UserService;
import com.binar.binarfud.users.utils.GenericResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    private final UserService userService;

    @PostMapping("/add")
    public GenericResponse<Object> addUser(@RequestBody UsersReqDTO usersReqDTO) {
        String addUser =  userService.addUser(usersReqDTO);
        return GenericResponse.success(HttpStatus.OK, addUser, null);
    }

    @PutMapping("/edit/{id}")
    public GenericResponse<Object> addUser(@PathVariable("id") UUID id, @RequestBody UsersReqDTO usersReqDTO) {
        return GenericResponse.success(HttpStatus.OK, userService.editUser(id, usersReqDTO), null);
    }

    @DeleteMapping("/delete/{id}")
    public GenericResponse<Object> deleteUser(@PathVariable("id") UUID id) {
        return GenericResponse.success(HttpStatus.OK, userService.deleteUser(id), null);
    }

}
