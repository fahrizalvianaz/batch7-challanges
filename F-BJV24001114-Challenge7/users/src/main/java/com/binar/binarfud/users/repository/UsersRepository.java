package com.binar.binarfud.users.repository;

import com.binar.binarfud.users.model.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UsersRepository extends JpaRepository<Users, UUID>{

    @Procedure(procedureName = "insert_users")
    void insertUser(
            @Param("usernamePr") String name,
            @Param("emailPr") String email,
            @Param("passPr") String password);


    @Procedure(procedureName = "edit_users")
    void editUser(
            @Param("idPr") UUID id,
            @Param("usernamePr") String name,
            @Param("emailPr") String email,
            @Param("passPr") String password);


    @Procedure(procedureName = "delete_users")
    void deleteUser(@Param("idPr") UUID id);

    Users findByEmailAddress(String email);


}
