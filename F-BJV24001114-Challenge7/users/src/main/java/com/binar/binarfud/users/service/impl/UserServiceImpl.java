package com.binar.binarfud.users.service.impl;

import com.binar.binarfud.users.config.BCrypt;
import com.binar.binarfud.users.dto.UsersReqDTO;
import com.binar.binarfud.users.exception.UnauthorizedException;
import com.binar.binarfud.users.model.entity.Users;
import com.binar.binarfud.users.model.entity.oauth.User;
import com.binar.binarfud.users.repository.UsersRepository;
import com.binar.binarfud.users.repository.oauth.UserRepository;
import com.binar.binarfud.users.service.UserService;
import com.binar.binarfud.users.service.oauth.Oauth2UserDetailsService;
import com.binar.binarfud.users.utils.Response;
import lombok.AllArgsConstructor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.security.Principal;
import java.util.Map;
import java.util.UUID;

@Service
@AllArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {


    private final UsersRepository userRepository;
    private final UserRepository usersRepository;





    @Override
    public String addUser(UsersReqDTO userRequest) {
        Users user = userRepository.findByEmailAddress(userRequest.getEmail());
        if(user != null) {
            throw new UnauthorizedException("Email sudah terdaftar");
        }

        String hashedPassword = BCrypt.hashpw(userRequest.getPassword(), BCrypt.gensalt());
        userRepository.insertUser(userRequest.getUsername(), userRequest.getEmail(), hashedPassword);
        return "User berhasil ditambahkan";
    }

    @Override
    public String editUser(UUID userCode, UsersReqDTO userRequest) {
        Users user = userRepository.findById(userCode)
                .orElseThrow(() -> new UnauthorizedException("User tidak ditemukan"));
        String hashedPassword = userRequest.getPassword() != null ? BCrypt.hashpw(userRequest.getPassword(), BCrypt.gensalt()) : null;
        userRepository.editUser(user.getId(), userRequest.getUsername(), userRequest.getEmail(), hashedPassword);
        return "User berhasil diupdate";
    }

    @Override
    public String deleteUser(UUID userCode) {
        Users user = userRepository.findById(userCode)
                .orElseThrow(() -> new UnauthorizedException("User tidak ditemukan"));
        userRepository.deleteUser(user.getId());
        return "User berhasil dihapus";
    }

    @Autowired
    private Oauth2UserDetailsService userDetailsService;

    @Autowired
    public Response templateResponse;

    @Override
    public Map getDetailProfile(Principal principal) {
        log.info("principal.getName() : " + principal);
        User idUser = getUserIdToken(principal, userDetailsService);
        try {
//            User obj = userRepository.save(idUser);
            return templateResponse.sukses(idUser);
        } catch (Exception e){
            return templateResponse.error(e,"500");
        }
    }

    private User getUserIdToken(Principal principal, Oauth2UserDetailsService userDetailsService) {
        UserDetails user = null;

        String username = principal.getName();
        if (!StringUtils.isEmpty(username)) {
            user = userDetailsService.loadUserByUsername(username);
        }

        if (null == user) {
            throw new UsernameNotFoundException("User not found");
        }
        User idUser = usersRepository.findOneByUsername(user.getUsername());
        if (null == idUser) {
            throw new UsernameNotFoundException("User name not found");
        }
        return idUser;
    }


}
