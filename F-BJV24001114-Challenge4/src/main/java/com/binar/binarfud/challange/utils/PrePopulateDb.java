package com.binar.binarfud.challange.utils;

import com.binar.binarfud.challange.model.entity.Merchant;
import com.binar.binarfud.challange.model.entity.Users;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class PrePopulateDb implements CommandLineRunner {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    public void run(String... args) throws Exception {

        for(int i = 1; i <= 10; i++) {
            Merchant merchant = new Merchant();
            merchant.setMerchantName("Merchant " + i);
            merchant.setMerchantLocation("Jalan Merchant " + i);
            merchant.setOpen(true);
            entityManager.persist(merchant);
        }

        Users user = new Users();
        user.setUsername("Fulan");
        user.setPassword("$2y$10$yLYY67MdglzK6DEEdkX6y.C4Moi2aMs3a8rRaWdGfdeSWoMqck.R6");
        user.setEmailAddress("fulan@gmail.com");
        entityManager.persist(user);

    }
}
