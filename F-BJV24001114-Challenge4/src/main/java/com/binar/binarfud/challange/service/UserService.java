package com.binar.binarfud.challange.service;

import com.binar.binarfud.challange.dto.UsersReqDTO;

import java.util.UUID;

public interface UserService {

    String addUser(UsersReqDTO userRequest);

    String editUser(UUID userCode, UsersReqDTO userRequest);

    String deleteUser(UUID userCode);

}
