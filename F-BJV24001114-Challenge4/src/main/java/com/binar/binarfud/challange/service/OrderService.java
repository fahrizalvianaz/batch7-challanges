package com.binar.binarfud.challange.service;

import com.binar.binarfud.challange.dto.OrderReqDTO;

import java.util.Map;
import java.util.UUID;

public interface OrderService {

    Map createOrder(OrderReqDTO orderData);

    Map editStatusOrder(UUID orderId, boolean isCompleted);

    Map getAllOrder();

    Map deteleOrder(UUID orderId);
}
