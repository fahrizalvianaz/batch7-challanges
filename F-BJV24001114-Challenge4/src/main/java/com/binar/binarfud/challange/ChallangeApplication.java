package com.binar.binarfud.challange;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition
public class ChallangeApplication {
	public static void main(String[] args) {
		SpringApplication.run(ChallangeApplication.class, args);
	}

}
