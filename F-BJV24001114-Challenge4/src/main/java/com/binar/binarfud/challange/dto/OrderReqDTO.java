package com.binar.binarfud.challange.dto;

import com.binar.binarfud.challange.model.entity.Users;
import jakarta.persistence.Column;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;

import java.util.UUID;

@Data
public class OrderReqDTO {

    private String destinationAddress;
    private UUID userId;
}
