package com.binar.binarfud.challange.controller;

import com.binar.binarfud.challange.dto.OrderReqDTO;
import com.binar.binarfud.challange.service.OrderService;
import com.binar.binarfud.challange.utils.Response;
import lombok.AllArgsConstructor;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.UUID;

@AllArgsConstructor
@RestController
@RequestMapping("/api/v1/order")
public class OrderController {

    private final OrderService orderService;
    private final Response response;
    @PostMapping(value = {"/add", "/add/"})
    public ResponseEntity<Map> saveEmployee(@RequestBody OrderReqDTO request) {
        return new ResponseEntity<Map>(response.sukses(orderService.createOrder(request)), HttpStatus.OK);
    }

    @PutMapping(value = {"/edit/{orderId}", "/edit/{orderId}/"})
    public ResponseEntity<Map> editStatusOrder(
            @PathVariable UUID orderId,
            @RequestParam boolean isCompleted) {
        return new ResponseEntity<Map>(response.sukses(orderService.editStatusOrder(orderId, isCompleted)), HttpStatus.OK);
    }

    @GetMapping(value = {"/orders", "/orders/"})
    public ResponseEntity<Map> getAllOrder() {
        return new ResponseEntity<Map>(response.sukses(orderService.getAllOrder()), HttpStatus.OK);
    }

    @DeleteMapping(value = {"/delete/{orderId}", "/delete/{orderId}/"})
    public ResponseEntity<Map> deleteOrder(@PathVariable UUID orderId) {
        return new ResponseEntity<Map>(response.sukses(orderService.deteleOrder(orderId)), HttpStatus.OK);
    }


}
