package com.binar.binarfud.challange.dto;

import com.binar.binarfud.challange.model.enums.Category;
import lombok.Data;

@Data
public class ProductReqDTO {
    private String productName;
    private double price;
    private Category category;
}
