package com.binar.binarfud.challange.model.entity;

import com.binar.binarfud.challange.model.enums.Category;
import jakarta.persistence.*;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
@Entity
@Table(name = "product", schema = "public")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    @Column(name = "product_code")
    private UUID productCode;

    @Column(name = "product_name")
    private String productName;

    private double price;

    private Category category;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "merchant_id")
    private Merchant merchantId;


}
