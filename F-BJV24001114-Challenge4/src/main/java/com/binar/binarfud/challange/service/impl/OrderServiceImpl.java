package com.binar.binarfud.challange.service.impl;

import com.binar.binarfud.challange.dto.OrderReqDTO;
import com.binar.binarfud.challange.exception.UnauthorizedException;
import com.binar.binarfud.challange.model.entity.Order;
import com.binar.binarfud.challange.model.entity.Users;
import com.binar.binarfud.challange.repository.OrderRepository;
import com.binar.binarfud.challange.repository.UsersRepository;
import com.binar.binarfud.challange.service.OrderService;
import com.binar.binarfud.challange.utils.Response;
import lombok.AllArgsConstructor;
import org.apache.catalina.User;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.UUID;

@Service
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final Response response;
    private final OrderRepository orderRepository;
    private final UsersRepository userRepository;
    @Override
    public Map createOrder(OrderReqDTO orderData) {
        Users user = userRepository.findById(orderData.getUserId())
                .orElseThrow(() -> new UnauthorizedException("User tidak ditemukan"));

        Order order = new Order();
        order.setDestinationAddress(orderData.getDestinationAddress());
        order.setUserId(user);
        order.setCompleted(false);

        return response.sukses(orderRepository.save(order));
    }


    @Override
    public Map editStatusOrder(UUID orderId, boolean isCompleted) {
        Order order = orderRepository.findById(orderId)
                .orElseThrow(() -> new UnauthorizedException("Order tidak ditemukan"));

        order.setCompleted(isCompleted);
        return response.sukses(orderRepository.save(order));
    }

    @Override
    public Map getAllOrder() {
        return response.sukses(orderRepository.findAll());
    }

    @Override
    public Map deteleOrder(UUID orderId) {
        Order order = orderRepository.findById(orderId)
                .orElseThrow(() -> new UnauthorizedException("Order tidak ditemukan"));
        orderRepository.delete(order);
        return response.sukses("Order berhasil dihapus");
    }
}
