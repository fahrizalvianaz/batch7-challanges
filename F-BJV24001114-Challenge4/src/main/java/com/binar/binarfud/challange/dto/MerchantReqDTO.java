package com.binar.binarfud.challange.dto;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MerchantReqDTO {

    private String merchantName;

    private String merchantAddress;


}