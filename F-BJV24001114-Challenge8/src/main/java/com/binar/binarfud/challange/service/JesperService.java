package com.binar.binarfud.challange.service;

import com.binar.binarfud.challange.model.entity.OrderDetail;

import java.util.List;

public interface JesperService {

    byte[] getItemReport(List<OrderDetail> items, String format);

}
