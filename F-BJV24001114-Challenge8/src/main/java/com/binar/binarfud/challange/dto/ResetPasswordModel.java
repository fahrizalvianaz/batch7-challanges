package com.binar.binarfud.challange.dto;

import lombok.Data;
 
 
@Data 
public class ResetPasswordModel { 
    public String email; 
 
    public String otp; 
    public String newPassword; 
} 