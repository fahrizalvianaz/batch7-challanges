package com.binar.binarfud.challange.service.impl;

import com.binar.binarfud.challange.dto.MerchantReqDTO;
import com.binar.binarfud.challange.exception.BadRequestException;
import com.binar.binarfud.challange.exception.UnauthorizedException;
import com.binar.binarfud.challange.model.entity.Merchant;
import com.binar.binarfud.challange.repository.MerchantRepository;
import com.binar.binarfud.challange.service.MerchantService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
@Slf4j
public class MerchantServiceImpl implements MerchantService {


    private final MerchantRepository merchantRepository;
    private static final Logger logger = LoggerFactory.getLogger(MerchantServiceImpl.class);


    @Override
    public String addMerchant(MerchantReqDTO merchantRequest) {

        Merchant merchant = new Merchant();
        merchant.setMerchantName(merchantRequest.getMerchantName());
        merchant.setMerchantLocation(merchantRequest.getMerchantAddress());
        merchant.setOpen(false);
        Merchant merchantSave = merchantRepository.save(merchant);
        logger.info("Merchant berhasil ditambahkan => " + merchantSave);
        return "Berhasil menambahkan merchant baru";
    }

    @Override
    public String editStatusMerchant(UUID merhcantCode, boolean isOpen) {
        Merchant merchant = merchantRepository.findById(merhcantCode)
                .orElseThrow(() -> new UnauthorizedException("Merchant tidak ditemukan"));

        merchant.setOpen(isOpen);
        merchantRepository.save(merchant);
        logger.info("Merchant berhasil dibuka");
        return "Merchant berhasil dibuka";
    }

    @Override
    public List<Merchant> getMerchant(boolean isOpen, int page, int size) {
        Pageable pageable = PageRequest.of(((page > 0) ? page - 1 : page), size);
        Page<Merchant> merchant = merchantRepository.findAllByOpenIs(isOpen, pageable);
        if (merchant.isEmpty()) {
            log.error("Merchant tidak ditemukan");
            throw new BadRequestException("Merchant tidak ditemukan");
        }
        logger.info("Merchant berhasil diambil");
        return merchant.getContent();
    }
}
