package com.binar.binarfud.challange.model.entity;


import javax.persistence.*;
import lombok.Data;


import java.util.UUID;

@Data
@Entity
@Table(name = "users", schema = "public")
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private UUID id;

    private String username;

    @Column(name = "email_address")
    private String emailAddress;

    private String password;

}
