package com.binar.binarfud.challange.service;


import com.binar.binarfud.challange.dto.MerchantReqDTO;
import com.binar.binarfud.challange.model.entity.Merchant;
import com.binar.binarfud.challange.repository.MerchantRepository;
import com.binar.binarfud.challange.service.impl.MerchantServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MerchantServiceTest {

    @Mock
    private MerchantRepository merchantRepository;

    @InjectMocks
    private MerchantServiceImpl merchantServiceImpl;


    @Test
    public void MerchantService_addMerchant_ReturnMerchantDTO() {
        Merchant merchant = new Merchant();
        merchant.setMerchantName("Merchant 1");
        merchant.setMerchantLocation("Jalan Merchant 1");

        MerchantReqDTO merchantReqDTO = MerchantReqDTO.builder()
                .merchantName("Merchant 1")
                .merchantAddress("Jalan Merchant 1")
                .build();

        when(merchantRepository.save(Mockito.any(Merchant.class))).thenReturn(merchant);

        String result = merchantServiceImpl.addMerchant(merchantReqDTO);
        Assertions.assertNotNull(result);
    }

    
    @Test
    public void MerchantService_GetAllMerchantByOpenIsTrue_ReturnMerchantDTO() {
        Merchant merchant = new Merchant();
        merchant.setMerchantName("Merchant 1");
        merchant.setMerchantLocation("Jalan Merchant 1");

        MerchantReqDTO merchantReqDTO = MerchantReqDTO.builder()
                .merchantName("Merchant 1")
                .merchantAddress("Jalan Merchant 1")
                .build();

        when(merchantRepository.save(Mockito.any(Merchant.class))).thenReturn(merchant);

        String result = merchantServiceImpl.addMerchant(merchantReqDTO);
        Assertions.assertNotNull(result);
    }
}
